# Aims

This repository contains a simple calculator to estimate the cost of membership of the British Library DataCite consortium.

# Current status

:green_apple: Functional and ready to eat

Launch the calculator using mybinder.org:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/bl-data-services%2F2019-datacite-cost-calculator/master?urlpath=%2Fvoila%2Frender%2Fcosting-dashboard.ipynb)

## Possible enhancements

- Visualisation of some kind
- Fetch usage via DataCite REST API instead of static file
  - Not necessary: the past data is closed and will not change
  - Could demonstrate use of API
- Allow data for non-BL organisations to be loaded
  - Would also need support for other currencies

# Files in this repository

| File                      | Description                                                             |
|---------------------------|-------------------------------------------------------------------------|
| `2019-usage.csv`          | Usage figures for each repository by ID                                 |
| `costing-dashboard.ipynb` | [Jupyter Notebook][] defining the calculator                            |
| `Pipfile`                 | Specification of required Python packages (see [Pipfile description][]) |
| `Pipfile.lock`            | Actual versions of packages used (machine-readable)                     |
| `README.md`               | This file                                                               |

[Jupyter Notebook]: https://jupyter.org/

[Pipfile description]: https://github.com/pypa/pipfile
